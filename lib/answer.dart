import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final VoidCallback selectQuestion;
  final String answerText;

  Answer(this.selectQuestion, this.answerText);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        color: Colors.deepPurple,
        textColor: Colors.amberAccent,
        child: Text(
          answerText,
          style: TextStyle(fontSize: 20),
        ),
        onPressed: selectQuestion,
      ),
    );
  }
}
