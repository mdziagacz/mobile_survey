import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int _totalScore;
  final VoidCallback _reset;

  Result(this._totalScore, this._reset);

  String get result {
    if (_totalScore > 100) return 'We called the police';
    if (_totalScore == 21)
      return 'Hello witch!!!';
    else
      return 'You\'re quite normal';
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Text(
        result,
        style: TextStyle(fontSize: 25),
        textAlign: TextAlign.center,
      ),
      SizedBox(
        height: MediaQuery.of(context).size.width * 0.6,
      ),
      Container(
          width: double.infinity,
          child: RaisedButton(
            child: Text('once again',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            onPressed: _reset,
            color: Colors.amberAccent,
          ))
    ]);
  }
}
